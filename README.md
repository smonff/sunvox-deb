# Sunvox debian packager

[Sunvox](http://www.warmplace.ru/soft/sunvox/) don't have packages for some reason. I wanted sunvox in my personal apt repo, but it's not distributed as such.

Fairly ashamed that this uses fpm, but it gets the job done until such
a time as I can do proper packaging.

## Install dependencies

    $ apt-get install ruby ruby-dev rubygems build-essential
    $ gem install --no-ri --no-rdoc fpm

## Build package

    $ make

## Install package

    $ sudo dpkg -i sunvox-1.9.4.deb 

# Authors

- kisom
- smonff

Originaly forked from https://github.com/kisom/sunvox-deb


